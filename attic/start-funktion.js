#!/usr/bin/env node

/*
./start-funktion.js hey 9099 30000
./start-funktion.js hello 9098 30000
*/
const spawn = require('child_process').spawn
const fs = require('fs')
const path = require('path')

// === Spawning child process ===

let project = process.argv[2]
let httPort= process.argv[3] //TODO: find the appropriate port
let lifeDelay= process.argv[4]

console.log(project, httPort, lifeDelay)

let RUNTIME_PATH = require('../store.json').RUNTIME_PATH
let FUNKTIONS_PATH = require('../store.json').FUNKTIONS_PATH


console.log(RUNTIME_PATH, FUNKTIONS_PATH)

let languages = {
	".js": "js", ".py": "python", ".rb": "ruby"
}

fs.readdir(`${FUNKTIONS_PATH}/${project}`, (err, items) => {
	let indexFile = items.find(file => file.startsWith("index"))
	
	let extension = path.extname(indexFile)
	let language = languages[extension]
	console.log(indexFile, extension, language)
	
	fs.readFile(`${FUNKTIONS_PATH}/${project}/${indexFile}`, 'utf8',  (error,sourceCode) => {
		if (error) { return console.log(error) }
		console.log(sourceCode)
		
		process.env.PORT = httPort
		process.env.LANG = language
		process.env.FUNCTION_CODE = sourceCode
		
		let child = spawn(`java`, ['-jar', RUNTIME_PATH], process.env)
		
		child.on('exit', _ => {
			clearTimeout(to)
			console.log('Child exited!')
		})
		
		child.stdout.on('data', (data) => {
			console.log('stdout: ' + data)
		})
		
		child.stderr.on('data', (data) => {
			console.log('stderr: ' + data)
		})
		
		let to = setTimeout( _ => {
			console.log('Sending sigkill')
			child.kill()
		}, lifeDelay) // it's milliseconds
		
	})
	
})
