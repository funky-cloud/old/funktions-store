// to execute this test run `node parent.js`

var spawn = require('child_process').spawn;

console.log('Spawning child process');

var child = spawn('node', ['child.js']);

child.on('exit', function(){
	clearTimeout(to);
	console.log('Child exited!');
});

child.stdout.on('data', function (data) {
	console.log('stdout: ' + data);
});

child.stderr.on('data', function (data) {
	console.log('stderr: ' + data);
});

var to = setTimeout(function(){
	console.log('Sending sigkill');
	child.kill();
}, 2000);