#!/bin/bash
eval $(cat store.config)
# --- usage ---
# ./start-funktion.sh hello 9098 60
# ./start-funktion.sh hey 9099 60
PROJECT=$1
PORT=$2
DELAY=$3

# TODO
# - detect language
# - find the source file

fullfile=$(ls ./funktions/$PROJECT/index.*)
filename=$(basename -- "$fullfile")
extension="${filename##*.}"

if [[ "$extension" == "js" ]]; then
 LANG="js"
fi

if [[ "$extension" == "rb" ]]; then
 LANG="ruby"
fi

if [[ "$extension" == "py" ]]; then
 LANG="python"
fi

echo "fullfile: $fullfile"
echo "filename: $filename"
echo "extension: $extension"

function deploy() {
  PORT=$PORT LANG=$LANG \
  FUNCTION_CODE="$(cat $fullfile)" \
  java -jar $RUNTIME_PATH & echo $!
}

function destroy() {
  echo "🖐️ function with id:$FUNCTION_ID will be destroyed in $1 secs."
  sleep $1
  kill -9 $2
  rm ./running/$PROJECT.$PORT
}

{
  deploy $PORT
} &> /dev/null

# TODO
# - call the health check etc ... to check if the function is ok
# - and display something
FUNCTION_ID=$!
echo "🚀 function with id:$FUNCTION_ID is started and is listening on http port: $PORT"
echo "" > ./running/$PROJECT.$PORT

destroy $DELAY $FUNCTION_ID &


