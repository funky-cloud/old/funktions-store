const express = require('express')
const fs = require('fs')
const path = require('path')
const spawn = require('child_process').spawn
const http = require('http')

const app = express()
//const http_port = process.env.HTTP_PORT || 8090

app.use(express.static('public'))
app.use(express.json())

// when starting poll all function and add httport
let RUNTIME_PATH = process.env.RUNTIME_PATH
let FUNKTIONS_PATH = process.env.FUNKTIONS_PATH
let HTTP_PORT_START = parseInt(process.env.HTTP_PORT_START)
let STORE_HTTP_PORT = parseInt(process.env.STORE_HTTP_PORT)
let LIFE_DELAY = parseInt(process.env.LIFE_DELAY)
let FUNKTIONS_STORE_PREFIX = process.env.FUNKTIONS_STORE_PREFIX

let languages = {
	".js": "js", ".py": "python", ".rb": "ruby", "kt": "kotlin"
}

var lastHttpPort = HTTP_PORT_START
let funktionsProjects = {}

let runningFunktions = {}

fs.readdir(`${FUNKTIONS_PATH}/`, (err, projects) => {
	
	projects.forEach(project => {

		projectFiles = fs.readdirSync(`${FUNKTIONS_PATH}/${project}`)
		let indexFile = projectFiles.find(file => file.startsWith("index"))
		let extension = path.extname(indexFile)
		let language = languages[extension]
		
		funktionsProjects[project] = {
			file: indexFile,
			language: language,
			extension: extension,
			port: lastHttpPort+=1,
			running: false
		}
		
	})
	
	console.log("Projects", funktionsProjects)
	
})


//TODO: add funktion project

// curl http://localhost:8090/funktions/list
app.get(`/${FUNKTIONS_STORE_PREFIX}/list`, (req, res) => {
	res.send(funktionsProjects)
})

// curl http://localhost:8090/funktions/hello
app.get(`/${FUNKTIONS_STORE_PREFIX}/:name`, (req, res) => {
	res.send(funktionsProjects[req.params.name])
})

// curl http://localhost:8090/funktions/start/hello
app.get(`/${FUNKTIONS_STORE_PREFIX}/:name/start`, (req, res) => {
	let funktionName = req.params.name
	let funktionLang = funktionsProjects[funktionName].language
	let funktionExtension = funktionsProjects[funktionName].extension
	let funktionPort = funktionsProjects[funktionName].port
	let funktionIsRunning = funktionsProjects[funktionName].running
	
	if(!funktionIsRunning) {
		fs.readFile(`${FUNKTIONS_PATH}/${funktionName}/index${funktionExtension}`, 'utf8',  (error,sourceCode) => {
			if (error) {
				return console.log(error)
				//res.send({error: error.message})
			}
			process.env.PORT = funktionPort
			process.env.LANG = funktionLang
			process.env.FUNCTION_CODE = sourceCode
			
			//console.log("🤖", process.env)
			
			let child = spawn(`java`, ['-Dfile.encoding=UTF8', '-jar', RUNTIME_PATH], process.env)
			funktionsProjects[funktionName].running = true
			
			res.send(funktionsProjects[funktionName])
			
			child.on('exit', _ => {
				clearTimeout(to)
				console.log('Child exited!')
			})
			
			child.stdout.on('data', (data) => {
				console.log('stdout: ' + data)
			})
			
			child.stderr.on('data', (data) => {
				console.log('stderr: ' + data)
			})
			
			let to = setTimeout( _ => {
				console.log('Sending sigkill')
				child.kill()
				funktionsProjects[funktionName].running = false
			}, LIFE_DELAY)
		})
	} else {
		res.send(funktionsProjects[funktionName])
	}
	
})

let httpServer = http.createServer(app)

httpServer.listen(STORE_HTTP_PORT, () => console.log(`🌍 webapp is listening on port ${STORE_HTTP_PORT}!`))





